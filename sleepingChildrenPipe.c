#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	pid_t id;
	int status;
	int p[2];

	
	// Creo el pipe
	pipe(p);



	// Ejecuto el primer proceso
	id = fork();
	if (id == -1) {
		perror("Error en fork");
		exit(EXIT_FAILURE);
	} else if (id == 0) {
		close(1);
		dup(p[1]);
		close(p[0]);
		close(p[1]);

		char *args[] = {NULL};
		printf("%d",execv("p.fl", args));
		perror("Error en exec");
		exit(EXIT_FAILURE);
	}


	// Ejecuto el segundo proceso
	id = fork();
	if (id == -1) {
		perror("Error en fork");
		exit(EXIT_FAILURE);
	} else if (id == 0) {
		close(0);
		dup(p[0]);
		close(p[0]);
		close(p[1]);

		char *args[] = {NULL};
		printf("%d",execv("c.fl", args));
		perror("Error en exec");
		exit(EXIT_FAILURE);
	}

	close(p[0]);
	close(p[1]);


	wait(&status);
	wait(&status);

	printf("Todos los hijos terminaron! Puedo terminar\n");
	exit(EXIT_SUCCESS);

}