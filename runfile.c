

int runfile(char *path) {
	id = fork();
	if (id == -1) {
		perror("Error en fork");
		exit(EXIT_FAILURE);
	} else if (id == 0) {
		char *args[] = {NULL};
		printf("%d",execv(path, args));
		perror("Error en exec");
		exit(EXIT_FAILURE);
	}
}