
echo "Starting compilation..."

gcc -Wall  -g -c sleepingChildrenPipe.c true.c false.c c.c p.c 
gcc -g  -Wall sleepingChildrenPipe.o -o sleepingChildrenPipe.fl
gcc -g  -Wall p.o -o p.fl
gcc -g  -Wall c.o -o c.fl
printf "	Compilation Done!\n\n"



if [[ $* == *--dev* ]]; then

	# DEV COMANDS
	cppcheck sleepingChildrenPipe.c
	read -p "Press any key to run program... "
	valgrind ./sleepingChildrenPipe.fl
else 
	# Regular Commands
	printf "Running program \n\n"
	./sleepingChildrenPipe.fl
fi

