#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int n = 3;
	pid_t id, ids[n];
	int status;
	char * childrenPath = "sleepingChildrenCode.fl";
	for (int i = 0; i < n; ++i)
	{
		id = fork();

		if (id == -1) {
			perror("Error en fork");
			exit(EXIT_FAILURE);
		}
		if (id == 0)
		{	
			char *args[] = {};
			execv(childrenPath, args);
		} else {
			ids[i] = id;
		}
	}

	for (int i = 0; i < n; ++i)
	{
		// WUNTRACED || WNOHANG || WCONTINUED, parecería andar  WUNTRACED Y WCONTINUED nomas
		waitpid(ids[i], &status, WCONTINUED);
	}

	printf("Todos los hijos terminaron! Puedo terminar\n");
	exit(EXIT_SUCCESS);

}