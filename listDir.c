#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>



void printDir(char * path, int depth) {
	DIR * dirPointer = opendir(path);
    struct dirent *dir;

    while((dir = readdir(dirPointer))) {
    	char *name = dir->d_name;
    	if (strcmp(".",name) != 0 && strcmp("..",name) != 0) {

    		char isFile = (dir->d_type == 8);
    		char isDir = (dir->d_type == 4);

    		
    		// Print self
    		if (isFile) {
    			printf("f");
    		} else if(isDir) {
    			printf("d");
    		}

            int i;
    		for (i = 0; i < depth; ++i) {
    			printf("\t");
    		}
    		printf("%s\n", name);

    		
    		// If Dir, recursively call on next path
    		if (isDir) {
    			char * newPath = (char *) malloc((1+strlen(path) +strlen(name)) * 2);
    			strcpy(newPath, path);
    			strcat(newPath, "/");
    			strcat(newPath, name);
    			printDir(newPath, depth+1);
                free(newPath);
    		}
    	}
    }
    return;
}


int main(int argc, char *argv[]) {
	// open y readDir
	printDir("/", 1);
    exit(EXIT_SUCCESS);
}