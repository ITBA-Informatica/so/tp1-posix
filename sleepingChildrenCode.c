#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
	srand(time(NULL) ^ (getpid()<<16));
	int r = rand();      // Returns a pseudo-random integer between 0 and RAND_MAX.
	printf("Soy el hijo con ID: %d, me pongo a esperar %d segundos\n", getpid(), r % 10);	
	sleep(r % 10);
	printf("Hijo: %d terminado\n", getpid());	
	exit(EXIT_SUCCESS);
}